from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# Chrome browser
driver = webdriver.Chrome(executable_path="D:/chromedriver_win32/chromedriver.exe")

driver.get("https://www.amazon.in/")
driver.maximize_window()
time.sleep(2)
driver.find_element_by_id("twotabsearchtextbox").send_keys("MS-Q18YNZA")
time.sleep(2)

driver.find_element_by_id("nav-search-submit-button").click()
time.sleep(2)

driver.find_element_by_xpath("//*[contains(text(),'AmazonBasics 1.5 Ton 5 Star Split Inverter AC (White)')] ").click()

time.sleep(2)
driver.close()
handle = driver.window_handles              # Handles all the opened browsers
for hand in handle:
    driver.switch_to.window(hand)
    print(driver.title)

price = driver.find_element_by_id("priceblock_ourprice").text
print("Price is - ",price)

scroll = driver.find_element_by_xpath("//*[@id='productDetails_techSpec_section_1']/tbody/tr[2]/td")
driver.execute_script("arguments[0].scrollIntoView();",scroll)

manufacturer = driver.find_element_by_xpath("//*[@id='productDetails_techSpec_section_1']/tbody/tr[15]/td").text
print("Manufacturer is - ",manufacturer)

dimensions = driver.find_element_by_xpath("//*[@id='productDetails_detailBullets_sections1']/tbody/tr[8]/td").text
print("Dimensions are - ",dimensions)

dfa = driver.find_element_by_xpath("//*[@id='productDetails_detailBullets_sections1']/tbody/tr[4]/td").text
print("Date first available is - ",dfa)

time.sleep(2)

add_to_cart = driver.find_element_by_id("add-to-cart-button")
driver.execute_script("arguments[0].scrollIntoView();",add_to_cart)
time.sleep(2)
add_to_cart.click()



